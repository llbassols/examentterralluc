package cat.dam.lluc.holanom;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//afegides les classes necessaries
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;
import android.view.View;
public class MainActivity extends AppCompatActivity {
    private EditText et_nom;
    private EditText et_contrasenya;
    private Button btn_ok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Obtenim una referència als controls de la interfície
        et_nom = (EditText)findViewById(R.id.activity_main_et_nom);
        et_contrasenya = (EditText)findViewById(R.id.activity_main_et_contrasenya);
        btn_ok = (Button)findViewById(R.id.activity_main_btn_ok);
        //Implementem l'esdeveniment click del botó
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creem l'Intent
                String contra = et_contrasenya.getText().toString();
                if (contra.equals("admin")){
                Intent intent =
                        new Intent(MainActivity.this, Main2Activity.class);
                //Creem la informació a passar entre activitats
                Bundle b = new Bundle();
                b.putString("NOM", et_nom.getText().toString());
                //Afegim la informació a l'intent
                intent.putExtras(b);
                //Iniciem la nova activitat
                startActivity(intent);
                }
            }
        });
    }
}